<?php

use App\Http\Controllers\TagController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:airlock')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'UserController@login');
Route::post('/register', 'UserController@register');
Route::get('/logout', 'UserController@logout');

Route::get('/companies', 'CompanyController@index');
Route::get('/companies/tag/{user}/{tag}', 'CompanyController@filterbytag');
Route::get('/companies/{name}/{rank}/{state}/{industry}/{revenue}/{growth}/{founded}/{workers}/{yearsOnList}/{metro}/{sortField}/{sortDirection}', 'CompanyController@filter');
Route::get('/company/{id}', 'CompanyController@show');

Route::middleware('auth:airlock')->delete('/tags/delete/{id}', 'TagController@destroy');
Route::middleware('auth:airlock')->get('/tags/{user}/{company}', 'TagController@index');
Route::middleware('auth:airlock')->get('/tags/{user}/{company}/{tag}', 'TagController@show');
Route::middleware('auth:airlock')->post('/tags', 'TagController@store');
