<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index()
    {
        $data = Company::paginate(
            15
        );
        return response()->json($data);
        //            ->header('Access-Control-Allow-Origin', '*')
        //            ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    }

    public function filter(
        $name,
        $rank,
        $state,
        $industry,
        $revenue,
        $growth,
        $founded,
        $workers,
        $yearsOnList,
        $metro,
        $sortField,
        $sortDirection
    ) {
        $nameFilter = $name === 'ANY' ? "%" : $name . "%";
        $stateFilter = $state === 'ANY' ? "%" : $state;
        $industryFilter = $industry === 'ANY' ? "%" : $industry . "%";
        $foundedFilter = $founded === 'ANY' ? "%" : $founded;
        $metroFilter = $metro === 'ANY' ? "%" : $metro . "%";
        $data = DB::table('companies')
            ->select('*')
            ->where('name', 'like', $nameFilter)
            ->where('id', '>=', $rank)
            ->where('state', 'like', $stateFilter)
            ->where('industry', 'like', $industryFilter)
            ->where('revenue', '>=', $revenue)
            ->where('growth', '>=', $growth)
            ->where('founded', 'like', $foundedFilter)
            ->where('workers', '>=', $workers)
            ->where('years_on_list', '>=', $yearsOnList)
            ->where('metro', 'like', $metroFilter)
            ->orderBy($sortField, $sortDirection)
            ->paginate(
                15
            );
        return response()->json($data);
        //    ->header('Access-Control-Allow-Origin', '*')
        //    ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    }

    public function filterbytag(
        $user,
        $tag
    ) {
        $data = DB::table('companies')
            ->select('*')
            ->join('tags', function ($join) use ($user, $tag) {
                $join->on('companies.id', '=', 'tags.company')
                    ->where('tags.user', '=', $user)
                    ->where('tags.tag', '=', $tag);
            })
            ->orderBy('tags.company', 'asc')
            ->paginate(
                15
            );
        return response()->json($data);
        //    ->header('Access-Control-Allow-Origin', '*')
        //    ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    }

    public function show($id)
    {
        $data = Company::find($id);
        return response()->json($data);
        //    ->header('Access-Control-Allow-Origin', '*')
        //    ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    }
}
