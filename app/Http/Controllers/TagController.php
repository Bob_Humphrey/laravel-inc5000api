<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TagController extends Controller
{
    public function index($user, $company)
    {
        $data = DB::table('tags')
            ->select('*')
            ->where('user', '=', $user)
            ->where('company', '=', $company)
            ->get();
        //Log::info("list function");
        //Log::info($data);
        if ($data) {
            return response()->json([
                'data' => $data,
                'error' => false
            ], 200);
        } else {
            return response()->json([
                'data' => [],
                'message' => "No tags were found",
                'error' => true
            ], 200);
        }
    }

    public function show($user, $company, $tag)
    {
        $data = DB::table('tags')
            ->select('*')
            ->where('user', '=', $user)
            ->where('company', '=', $company)
            ->where('tag', '=', $tag)
            ->get();
        //Log::info("list function");
        //Log::info($data);
        if ($data) {
            return response()->json([
                'data' => $data,
                'error' => false
            ], 200);
        } else {
            return response()->json([
                'data' => [],
                'message' => "No tags were found",
                'error' => true
            ], 200);
        }
    }

    public function store(Request $request)
    {
        Log::info("add function");
        $id = DB::table('tags')->insertGetId(
            [
                'user' => $request->user,
                'company' => $request->company,
                'tag' => $request->tag
            ]
        );
        if ($id) {
            return response()->json([
                'message' => 'The tag has been added.',
                'error' => false
            ], 201);
        } else {
            return response()->json([
                'message' => 'The tag could not be added.',
                'error' => true
            ], 422);
        }
    }

    public function destroy(Request $request, $id)
    {
        Log::info("delete function");
        $success = Tag::destroy($id);
        if ($success) {
            return response()->json([
                'message' => "Tag deleted",
                'error' => false
            ], 200);
        } else {
            return response()->json([
                'data' => [],
                'message' => "Tag not deleted",
                'error' => true
            ], 200);
        }
    }
}
